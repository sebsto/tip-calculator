//
//  TipGeoLocalizer.h
//  TipCalculator
//
//  Created by Sébastien Stormacq on 24/09/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>


@interface TipGeoLocalizer : NSObject <CLLocationManagerDelegate, MKReverseGeocoderDelegate> {

	CLLocation*		location;
	MKPlacemark*	placemark;
	NSError*		placemarkError;
	
	BOOL			geoLocalizationAuthorized;
	
@private
	MKReverseGeocoder* reverseGeocoder;
	
}

@property (retain) CLLocation*   location;
@property (retain) MKPlacemark*  placemark;
@property (retain) NSError*		placemarkError;

- (BOOL)isGeoLocalizationEnabled;
- (BOOL)isGeoLocalizationAuthorized;
- (void)findLocation;
- (void)findCountry;

//method from the CLLocationManagerDelegate protocol
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation;

//method for the MKReverseGeocoderDelegate protocol
- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error;
- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark;


@end
