//
//  TipCalculatorViewController.h
//  TipCalculator
//
//  Created by Sébastien Stormacq on 01/09/09.
//  Copyright Sun Microsystems 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <MessageUI/MessageUI.h>

@interface TipCalculatorViewController : UIViewController <UITextFieldDelegate, 
														   UIAlertViewDelegate, 
														   MFMailComposeViewControllerDelegate> {

	UITextField *amountRaw;
    UITextField *amountTipOnly;
	UITextField *amountWithTip;
	UITextField *amountPerPerson;
	
	UISlider *tip;
	UISlider *persons;
	
	UILabel *tipValue;
    UILabel *personsValue;
	
	UIButton* tipUsageInfo;
	UIActivityIndicatorView* tipUsageInfoWait;
	UILabel* tipFeedback;	

															   
    //will hold the tip list for this session
    //should we save this once downloaded ?
    //if yes, when to refresh it ?
    NSDictionary* tipList;
															   
	//a flag to indicate wether we already fetch the tip for this country (to avoid loading it a second time)
    float geoTipValue;
}

@property (nonatomic, retain) IBOutlet UITextField *amountRaw;
@property (nonatomic, retain) IBOutlet UITextField *amountTipOnly;
@property (nonatomic, retain) IBOutlet UITextField *amountWithTip;
@property (nonatomic, retain) IBOutlet UITextField *amountPerPerson;

@property (nonatomic, retain) IBOutlet UISlider *tip;
@property (nonatomic, retain) IBOutlet UISlider *persons;

@property (nonatomic, retain) IBOutlet UILabel *personsValue;
@property (nonatomic, retain) IBOutlet UILabel *tipValue;

@property (nonatomic, retain) IBOutlet UIButton *tipUsageInfo;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView* tipUsageInfoWait;
@property (nonatomic, retain) IBOutlet UILabel* tipFeedback;

@property (nonatomic, retain) NSDictionary* tipList;


- (void)computeValues;
- (void)sliderChanged:(id)sender;
- (void)tipUsageInfoRequested:(id)sender;
- (NSString*) formatTipValue:(float)fTip;

//method for the UIAlertViewDelegate protocol
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

//method for the MFMailMailComposeViewControlerDelegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error;

@end

