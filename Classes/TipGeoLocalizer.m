//
//  TipGeoLocalizer.m
//  TipCalculator
//
//  Created by Sébastien Stormacq on 24/09/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import "TipGeoLocalizer.h"


@implementation TipGeoLocalizer

@synthesize location;
@synthesize placemark;
@synthesize placemarkError;

-(id) init {
	geoLocalizationAuthorized = YES;
	
	return [super init];
}

- (BOOL)isGeoLocalizationEnabled {
	CLLocationManager* locationManager = [[[CLLocationManager alloc] init] autorelease];
	return locationManager.locationServicesEnabled;
}

- (BOOL)isGeoLocalizationAuthorized {
	return geoLocalizationAuthorized;
}


//TODO : handle case where location service id disabled
-(void)findLocation {
	
	// Create the location manager if this object does not
	// already have one.
	if (!location) {
		NSLog(@"Trying to get location");
		CLLocationManager* locationManager = [[[CLLocationManager alloc] init] autorelease];
		
		//DEBUG 
		NSLog(@"GeoLocalizationService are %@enabled", ([self isGeoLocalizationEnabled] ? @"" : @"NOT "));
		if ([self isGeoLocalizationEnabled]) {

			locationManager.delegate = self;
			locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
			
			// Set a movement threshold for new events
			locationManager.distanceFilter = 1000;
			
			assert(nil != locationManager);
		}	[locationManager startUpdatingLocation]; //async call, will wait later

	}
	
}

-(void)findCountry {
	
	//create a reverse geocoder and start updating our delegate
	if (!reverseGeocoder) {
		NSLog(@"Create and start ReverseGeoCoder");
		reverseGeocoder = [[[MKReverseGeocoder alloc] initWithCoordinate:location.coordinate] autorelease];
		reverseGeocoder.delegate = self;
		[reverseGeocoder start];
	} else {
		NSLog(@"ReverseGeoCoder already created/started, do nothing");
	}
}	

// Delegate method from the CLLocationManagerDelegate protocol.
//this method is called from the main (UI) thread
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    // If it's a relatively recent event, turn off updates to save power
	//NSLog(@"Received location event");
    NSDate* eventDate = newLocation.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 5.0) {
		NSLog(@"locationLanager did receive a recent notification : %@", newLocation);
        [manager stopUpdatingLocation];
		self.location = newLocation;
    }
    // else skip the event and process the next one.
	
}

//TODO
// Called when there is an error getting the location
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	NSMutableString *errorString = [[[NSMutableString alloc] init] autorelease];
	
	if ([error domain] == kCLErrorDomain) {
		
		// We handle CoreLocation-related errors here
		
		switch ([error code]) {
				// This error code is usually returned whenever user taps "Don't Allow" in response to
				// being told your app wants to access the current location. Once this happens, you cannot
				// attempt to get the location again until the app has quit and relaunched.
				//
				// "Don't Allow" on two successive app launches is the same as saying "never allow". The user
				// can reset this for all apps by going to Settings > General > Reset > Reset Location Warnings.
				//
			case kCLErrorDenied:
				[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationDenied", nil)];
				geoLocalizationAuthorized = NO;
				break;
				
				// This error code is usually returned whenever the device has no data or WiFi connectivity,
				// or when the location cannot be determined for some other reason.
				//
				// CoreLocation will keep trying, so you can keep waiting, or prompt the user.
				//
			case kCLErrorLocationUnknown:
				[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
				break;
				
				// We shouldn't ever get an unknown error code, but just in case...
				//
			default:
				[errorString appendFormat:@"%@ %d\n", NSLocalizedString(@"GenericLocationError", nil), [error code]];
				break;
		}
	} else {
		// We handle all non-CoreLocation errors here
		// (we depend on localizedDescription for localization)
		[errorString appendFormat:@"Error domain: \"%@\"  Error code: %d\n", [error domain], [error code]];
		[errorString appendFormat:@"Description: \"%@\"\n", [error localizedDescription]];
	}

	NSLog(errorString);
}

//delegate method from MKReverseGeocoderDelegate
- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error {
	NSLog(@"reverseGeocoder did received an error : %@", error);
	self.placemarkError = error;
}

//delegate method from MKReverseGeocoderDelegate
- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)newPlacemark {
	NSLog(@"reverseGeocoder did find placemark : %@, %@ (%@)", newPlacemark.locality, newPlacemark.country, newPlacemark.countryCode);
	self.placemark = newPlacemark;
}

-(void) dealloc {
	
	[self setLocation:nil];
	[self setPlacemark:nil];
	[self setPlacemarkError:nil];
	[super dealloc];
}

@end
