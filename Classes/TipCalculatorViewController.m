//
//  TipCalculatorViewController.m
//  TipCalculator
//
//  Created by Sébastien Stormacq on 01/09/09.
//  Copyright Sun Microsystems 2009. All rights reserved.
//

#import "TipCalculatorViewController.h"
#import "TipCalculatorAlertView.h";
#import "TipGeoLocalizer.h"
#import "Reachability.h"

@implementation TipCalculatorViewController 

@synthesize amountRaw;
@synthesize amountTipOnly;
@synthesize amountWithTip;
@synthesize amountPerPerson;

@synthesize tip;
@synthesize persons;

@synthesize tipValue;
@synthesize personsValue;

@synthesize tipUsageInfo;
@synthesize tipUsageInfoWait;
@synthesize tipFeedback;

@synthesize tipList;

//values for defaults, aka user settings
#define AMOUNT @"amount.prefs"
#define TIP @"tip.prefs"
#define PERSONS @"persons.prefs"

#ifdef DEBUG
#define TIP_LIST_URL @"TipList.plist"
#else
#define TIP_LIST_URL @"http://web.mac.com/sebsto/TipCalculator/TipList.plist"
#endif



/*
- (BOOL)textFieldShouldClear:(UITextField *)textField {

	//this will cause the keyboard to disapear
	[amountRaw resignFirstResponder];
	return NO;
}
 */

/**
 *
 * User has modified the value of the Amount TextField
 *
 */
- (void)textFieldDidEndEditing:(UITextField *)textField {

	//text has changed
	
	//compute amount with tip and amount per person
	[self computeValues];	
}

/**
 *
 * One of the two sliders has been adjusted
 *
 */

- (void)sliderChanged:(id)sender {
	
	//one of the two slider has changed
	if (sender == tip) {
		//[tipValue setText:[NSString stringWithFormat:@"%2.2f%%", [tip value]]];
		[tipValue setText:[self formatTipValue:floorf([tip value])]];
	}
	
	if (sender == persons) {
		int iPersons = [persons value]; //float to int implicit cast !
		[personsValue setText:[NSString stringWithFormat:@"%d", iPersons]];
	}
	
	//compute amount with tip and amount per person
	[self computeValues];	
	
}

/**
 *
 * User touched the screen
 * I am using this to dispose the keyboard when user is finished entering an amount
 *
 */

- (void) touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event {
	

	/*
    NSLog(@"touchesEnded");
    UITouch       *touch = [touches anyObject];
	
    if ([touch tapCount] == 2) {
        NSLog(@"double tap");
    }
	*/
	
	if ([amountRaw isFirstResponder]) {
		//this will cause the keyboard to disapear
		[amountRaw resignFirstResponder];	
	}
}

/**
 * This is THE business method of this application
 */
- (void)computeValues {

	float fAmount = [[amountRaw text] floatValue];
	float fTip = floorf([tip value]);
	float fAmountTipOnly = (fAmount * fTip / 100);
	float fAmountWithTip = fAmount + fAmountTipOnly;
	
	[amountTipOnly setText:[NSString stringWithFormat:@"%.2f", fAmountTipOnly]];
	[amountWithTip setText:[NSString stringWithFormat:@"%.2f", fAmountWithTip]];
	
	int iPersons = [persons value]; //float to int implicit cast !
	
	[amountPerPerson setText:[NSString stringWithFormat:@"%.2f", fAmountWithTip / iPersons]];
	
	//save user defaults
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:[amountRaw text] forKey:AMOUNT];
	[defaults setFloat:floorf([tip value]) forKey:TIP];
	[defaults setInteger:[persons value] forKey:PERSONS]; //flot to int implicit cast !!
	
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
	
	[super loadView];
		
	 //start the thread to receive the name of the country
	 //I do not reset the location, placemark and placemarkError as these should be nil
	 [self performSelectorInBackground:@selector(findCountryName:) withObject:nil];
 }


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
    [super viewDidLoad];
	
	tipUsageInfo.hidden = YES;
	[tipUsageInfoWait startAnimating];

	//register this as delegate to receive Text Input notification (return and clear)
	amountRaw.delegate = self;
	
	//indicate that we don't have geo tipping info yet
	geoTipValue = -1;
	
	//add this object as receiver for the slider events
	[tip addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
	[persons addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
	
	//add this object as receiver for the button (TipUsageInfo) events
	[tipUsageInfo addTarget:self action:@selector(tipUsageInfoRequested:) forControlEvents:UIControlEventTouchUpInside];
	
	//load user preferences
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	if (defaults) {
		//NSLog(@"Reading standardUserDefaults");
		
		//amount
		[amountRaw setText:[defaults stringForKey:AMOUNT]];
		
		//tip slider and label
		float fTip = [defaults floatForKey:TIP];
		[tip setValue:fTip];
		[tipValue setText:[self formatTipValue:fTip]];
		//NSLog(@"UserDefaults TIP = %2.2f%%", fTip);
		
		//persons slider and label
		int iPersons = [defaults integerForKey:PERSONS]; //int to float implicit cast !!
		//NSLog(@"UserDefaults PERSONS = %d", iPersons);
		if (iPersons == 0)
			iPersons = 1;
		[persons setValue:iPersons];
		[personsValue setText:[NSString stringWithFormat:@"%d", iPersons]];
		//NSLog(@"UserDefaults PERSONS = %d", iPersons);
		
		
		//refresh computed values
		[self computeValues];
		
	} else {
		NSLog(@"Can not load standardUserDefaults");
	}
	
}


/**
 * User pressed the Request Tip Usage Info button
 */
- (void)tipUsageInfoRequested:(id)sender {

	
	if (geoTipValue <= 0) {
		tipUsageInfo.hidden = YES;
		[tipUsageInfoWait startAnimating];
		
		//load data and get geo localization in a separate thread
		[self performSelectorInBackground:@selector(loadTipUsageData:) withObject:nil];
		
	} else {
	
		//adjust the slider 
		[tip setValue:geoTipValue];
		[self sliderChanged:tip];
	}
	
	
}

- (void)loadTipUsageData: (id)data {
	
	//THIS METHOD IS INVOKED ON A SEPARATE THREAD, HENCE REQUIRES AN AUTO RELEASE POOL
	NSLog(@"START OF THREAD");
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	NSRunLoop* loop = [NSRunLoop currentRunLoop];

	
//	TipGeoLocalizer* geo = [[[TipGeoLocalizer alloc] initWithLocation:location placemark:placemark andError:placemarkError] autorelease];
	TipGeoLocalizer* geo = [[[TipGeoLocalizer alloc] init] autorelease];
	
	// 1. Get geo coordinates (this object is create in th eain UI thread
	// to ensure we do receive the delegate's notifications
	[geo findLocation];

	
	// 2. Get the list of tip value per country 
	NSLog(@"Reading TipList from %@", TIP_LIST_URL);
	NSURL* url = [NSURL URLWithString:TIP_LIST_URL]; 
	assert(url != nil);
	
	if (!self.tipList) {

		//tip list is loaded only once per application run
		//TODO : should we cache to avoid fetching at each run ?
		self.tipList = [NSDictionary dictionaryWithContentsOfURL:url]; //sync

		//NSLog(@"Got TipList Dictionary %@", self.tipList);
		
		if (!self.tipList) {
			//error : can not load the tip list
			self.tipFeedback.text = NSLocalizedString(@"Can not load worlwide tip list", @"Can not load worlwide tip list");
			self.tipFeedback.textColor = [UIColor orangeColor];
			
			//STOP ANIMATION AND RELEASE POOL !! (this code is duplicated from the end of this method) .... bad bad
			[tipUsageInfoWait stopAnimating];
			tipUsageInfo.hidden = NO;
			[pool release];
			
			return;
		}
	}
	
	
	// 3. Get reverse geocoding to get the country name and code	
	//    (and wait to get the location)

	if (!geo.placemark) {

		//stop when we have a location and either a placemark or an error
		while (! (geo.location && (geo.placemark || geo.placemarkError)) ) {

			//wait the location data 
			NSLog(@"Going to wait for location");
			
			//allow the run loop to fetch events
			[loop runUntilDate:[NSDate dateWithTimeIntervalSinceNow:1]];

			//when we have location, fetch the address
			if (geo.location) {
				
				NSLog(@"Going to search address");
				[geo findCountry];
			}

		}
		NSLog(@"Exit thread loop - statuses are : \nlocation = %@\nplacemark = %@\nplacemarkError = %@", geo.location, geo.placemark, geo.placemarkError);
	}	

	// 4. Set the tip to the value found
	if (geo.placemarkError) {
		
		//error when doing the reverse geocoding
		self.tipFeedback.text = [NSString stringWithFormat:NSLocalizedString(@"Can not find your location : %@", @"Can not find your location : %@"), [geo.placemarkError localizedDescription]];
		self.tipFeedback.textColor = [UIColor orangeColor];
		
	} else {
		
		NSNumber* usualTip = [self.tipList objectForKey:geo.placemark.countryCode];
		
		if (usualTip) {
			
			if ([usualTip floatValue] == 0) {
				//no tip
				self.tipFeedback.text = [NSString stringWithFormat:NSLocalizedString(@"Tipping is not customary in %@", @"Tipping is not customary in %@"), geo.placemark.country];
			}
			else if ([usualTip floatValue] < 0) {
				//tip is uncommon but rounding is OK
				self.tipFeedback.text = [NSString stringWithFormat:NSLocalizedString(@"Altough tipping is not a general habit, rounding is considered polite in %@", @"Altough tipping is not a general habit, rounding is considered polite in %@"), geo.placemark.country];
			}
			else {	
				//give tip amount
				self.tipFeedback.text = [NSString stringWithFormat:NSLocalizedString(@"Usual tipping in %@ is %@ %%", @"Usual tipping in %@ is %@ %%"), geo.placemark.country, usualTip];
				geoTipValue = [usualTip floatValue];
			}
		} else {
			//we don't have tip information for this country
			self.tipFeedback.text = [NSString stringWithFormat:NSLocalizedString(@"No tipping information available for %@", @"No tipping information available for %@"), geo.placemark.country];
			
			//propose the user to send me a mail with the tipping info
			if ([MFMailComposeViewController canSendMail]) {
				NSString* title = NSLocalizedString(@"Question", @"Question");
				NSString* msg = [NSString stringWithFormat:NSLocalizedString(@"No tipping information available for %@, do you want to improve this application and send us an email with the tipping habits in %@ ?", @"No tipping information available for %@, do you want to improve this application and send us an email with the tipping habits in %@ ?"), geo.placemark.country, geo.placemark.country];

				TipCalculatorAlertView* alert = [[TipCalculatorAlertView alloc] init]; //will be release when user press a button (see delegate's method)
				alert.title = title;
				alert.message = msg;
				alert.delegate = self;
				alert.countryCode = geo.placemark.countryCode;
				alert.countryName = geo.placemark.country;
				[alert addButtonWithTitle:NSLocalizedString(@"Yes", @"Yes")];
				[alert addButtonWithTitle:NSLocalizedString(@"No", @"No")];
				 alert.cancelButtonIndex = 1;		 
				[alert show];
			} else {
				NSLog(@"Mail is disabled, will not propose to send the tip info");
			}
		}
	}
	
	
	
	[tipUsageInfoWait stopAnimating];
	tipUsageInfo.hidden = NO;
	
	[pool release];
	
	NSLog(@"END OF THREAD");
}


/**
 * This methods returns the country name
 * it is called in a separate thread at startup time
 *
 */
- (void)findCountryName: (id)data {
	
	//THIS METHOD IS INVOKED ON A SEPARATE THREAD, HENCE REQUIRES AN AUTO RELEASE POOL
	NSLog(@"START OF THREAD");
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	NSRunLoop* loop = [NSRunLoop currentRunLoop];
	
	//first check if network is available
	Reachability* reachability = [Reachability reachabilityForInternetConnection];
	if ([reachability currentReachabilityStatus] == NotReachable) {

		self.tipFeedback.text = [NSString stringWithFormat:NSLocalizedString(@"Please activate network to get geolocalized tipping information", @"Please activate network to get geolocalized tipping information")];
		self.tipUsageInfo.enabled = NO;

	} else {

		//create our geo helper.  This object will update location, placemark and placemarkError
		TipGeoLocalizer* geo = [[[TipGeoLocalizer alloc] init] autorelease];

		// 1. Get geo coordinates (this object is create in th eain UI thread
		// to ensure we do receive the delegate's notifications
		[geo findLocation];
		
		// 2. Get reverse geocoding to get the country name and code	
		//    (and wait to get the location)
		
		
		//stop when we have a location and either a placemark or an error
		while (! (geo.location && (geo.placemark || geo.placemarkError) || ![geo isGeoLocalizationAuthorized] || ![geo isGeoLocalizationEnabled]) ) {
			
			//wait the location data 
			NSLog(@"Going to wait for location");
			
			//allow the run loop to fetch events
			[loop runUntilDate:[NSDate dateWithTimeIntervalSinceNow:1]];
			
			//when we have location, fetch the address
			if (geo.location) {
				
				NSLog(@"Going to search address");
				[geo findCountry];
			}
			
		}
		NSLog(@"Exit thread loop - statuses are : \nlocation = %@\nplacemark = %@\nplacemarkError = %@", geo.location, geo.placemark, geo.placemarkError);
		
		
		// 4. Set the tip to the value found
		if (geo.placemarkError) {
			
			//error when doing the reverse geocoding
			//do not display any error message during this phase (startup time)
			NSLog(@"We got an error while retrieving country name - ignore for now");
			
		} else if (![geo isGeoLocalizationAuthorized]) {

			self.tipFeedback.text = NSLocalizedString(@"Authorize GeoLocalization services to get customary tip", @"Authorize GeoLocalization services to get customary tip");
			[tipUsageInfo setEnabled:NO];

		} else if (![geo isGeoLocalizationEnabled]) {
		
			self.tipFeedback.text = NSLocalizedString(@"Activate GeoLocalization services to get customary tip", @"Activate GeoLocalization services to get customary tip");
			[tipUsageInfo setEnabled:NO];

		} else {
			
			self.tipFeedback.text = [NSString stringWithFormat:NSLocalizedString(@"Click to get customary tip in %@", @"Click to get customary tip in %@"), geo.placemark.country];
			
		}
	}
	
	[tipUsageInfoWait stopAnimating];
	tipUsageInfo.hidden = NO;	
	[pool release];
	
	NSLog(@"END OF THREAD");
}

/**
 * This method is called when the user interacts with the UIAlertView
 *
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

	NSLog(@"Alert View clicked with button %d", buttonIndex);
	
	NSString* countryCode = ((TipCalculatorAlertView*)alertView).countryCode;
	NSString* countryName = ((TipCalculatorAlertView*)alertView).countryName;
	
	
	[alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
	[alertView release];
	
	if (buttonIndex == 0) {
		//send an email
		MFMailComposeViewController* mailer = [[MFMailComposeViewController alloc] init];
		[mailer setSubject:@"TipCalculator - Tipping Info"];
		[mailer setToRecipients:[NSArray arrayWithObject:@"tipcalculator@stormacq.com"]];
		[mailer setMessageBody:[NSString stringWithFormat:@"Country Code = %@ \n Country = %@ \n", countryCode, countryName] isHTML:NO];
		mailer.mailComposeDelegate = self;
		[self presentModalViewController:mailer animated:YES];
	}
}

/**
 * This method is called when the compose mail view is closed
 */
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {

	[self dismissModalViewControllerAnimated:YES];
	[controller release];
}


/**
 * Utility method to prepare the String representation of the tip value
 *
 */
- (NSString*) formatTipValue:(float)fTip {
	NSString* format = [NSString stringWithFormat:@"%2.0f %%", fTip];
	return format;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
	[self setTipList:nil];
    [super dealloc];
}

@end
