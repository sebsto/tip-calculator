//
//  TipCalculatorAlertView.h
//  TipCalculator
//
//  Created by Sébastien Stormacq on 24/09/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TipCalculatorAlertView : UIAlertView {

	NSString* countryName;
	NSString* countryCode;
}

@property (nonatomic, retain) NSString* countryName;
@property (nonatomic, retain) NSString* countryCode;


@end
