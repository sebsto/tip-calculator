//
//  TipCalculatorAlertView.m
//  TipCalculator
//
//  Created by Sébastien Stormacq on 24/09/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import "TipCalculatorAlertView.h"


@implementation TipCalculatorAlertView

@synthesize countryName;
@synthesize countryCode;

- (void) dealloc {
	[self setCountryCode:nil];
	[self setCountryName:nil];
	[super dealloc];
}

@end
