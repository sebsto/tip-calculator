//
//  TipCalculatorAppDelegate.h
//  TipCalculator
//
//  Created by Sébastien Stormacq on 01/09/09.
//  Copyright Sun Microsystems 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TipCalculatorViewController;

@interface TipCalculatorAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    TipCalculatorViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet TipCalculatorViewController *viewController;

@end

